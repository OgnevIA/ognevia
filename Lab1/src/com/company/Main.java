package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        for (int a = 0; a < args.length; a += 2) {
            switch (args[a]) {
                case "phone":
                    Phone[] phones = new Phone[Integer.parseInt(args[a + 1])];
                    for (int i = 0; i < Integer.parseInt(args[a + 1]); i++) {
                        phones[i] = new Phone();
                        phones[i].update();
                        phones[i].read();
                        System.out.println();
                    }
                    break;
                case "smart":
                    Smart[] smarts = new Smart[Integer.parseInt(args[a + 1])];
                    for (int i = 0; i < Integer.parseInt(args[a + 1]); i++) {
                        smarts[i] = new Smart();
                        smarts[i].update();
                        smarts[i].read();
                        System.out.println();
                    }
                    break;
                case "tablet":
                    Tablet[] tablets = new Tablet[Integer.parseInt(args[a + 1])];
                    for (int i = 0; i < Integer.parseInt(args[a + 1]); i++) {
                        tablets[i] = new Tablet();
                        tablets[i].update();
                        tablets[i].read();
                        System.out.println();
                    }
                    break;
                default:
                    System.out.println("Вводите по типу 'Название - Количество'");
                    break;
            }
        }
    }
}
