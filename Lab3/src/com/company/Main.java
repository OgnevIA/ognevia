package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Main {
   public static void main(String[] args) {
      Phone phone;
      Smart smart;
      Tablet tablet;
      ShoppingCart<Device> cart = new ShoppingCart();
      Order order = new Order();
      Orders<Order> orders = new Orders();
      while (true) {
         Scanner scanner = new Scanner(System.in);
         System.out.println("1 - Добавить в корзину\n2 - Удалить из корзины\n3 - Просмотреть корзину\n4 - Оформить заказ\n5 - Просмотреть список заказов\n6 - Найти по ID\n0 - Выход");
         int choice = scanner.nextInt();
         switch (choice) {
            case 1:
               int ch;
               System.out.println("Выберите тип девайса:\n1 - Телефон\n2 - Смартфон\n3 - Планшет");
               ch = scanner.nextInt();
               switch (ch) {
                  case 1:
                     phone = new Phone();
                     phone.create();
                     cart.add(phone);
                     break;
                  case 2:
                     smart = new Smart();
                     smart.create();
                     cart.add(smart);
                     break;
                  case 3:
                     tablet = new Tablet();
                     tablet.create();
                     cart.add(tablet);
                     break;
                  default:
                     System.out.println("Девайс не найден");
                     break;
               }
               break;
            case 2:
               cart.show();
               System.out.println("Выберите товар, который хотите удалить");
               int c = scanner.nextInt();
               cart.delete(c);
               break;
            case 3:
               cart.show();
               break;
            case 4:
               order.checkout(cart);
               orders.add(order);
               cart = new ShoppingCart();
               order = new Order();
               break;
            case 5:
               orders.show();
               break;
            case 6:
               System.out.println("Введите ID:");
               String buf = scanner.nextLine();
               String id = scanner.nextLine();
               cart.search(id);
               break;
            case 0:
               System.exit(0);
               break;
            default:
               continue;
         }
      }
   }
}
