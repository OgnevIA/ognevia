package com.check;

import com.demo.company.Order;
import com.demo.company.Orders;

/**
 * Класс, обеспечивающий поток для изменения статуса заказа по истечению времени ожидания
 */
public class ACheckStatus extends ACheck {

    public ACheckStatus() {
        super();
    }

    public ACheckStatus(Orders<Order> orders) {
        super(orders);
    }

    @Override
    public void run() {
        synchronized (orders) {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).changeStatus();
            }
        }
    }
}
