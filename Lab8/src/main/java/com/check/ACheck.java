package com.check;

import com.demo.company.Order;
import com.demo.company.Orders;

/**
 * Абстрактный класс для создания отдельных потоков
 */
public abstract class ACheck implements Runnable {
    protected Orders<Order> orders;
    protected Thread t;

    /**
     * Конструктор без параметров
     * Инициализация полей класса
     * Запуск потока
     */
    public ACheck() {
        orders = new Orders<>();
        t = new Thread(this);
        t.start();
    }

    /**
     * Конструктор с параметром
     * @param orders - Передаваемый контейнер заказов
     */
    public ACheck(Orders<Order> orders) {
        this.orders = orders;
        t = new Thread(this);
        t.start();
    }

    /**
     * Геттер потока
     * @return
     */
    public Thread getThread() {
        return t;
    }
}
