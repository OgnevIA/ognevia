package com.check;

import com.demo.company.Order;
import com.demo.company.Orders;

/**
 * Класс, обеспечивающий поток для удаления обработанных заказов
 */
public class ACheckDelete extends ACheck {

    public ACheckDelete() {
        super();
    }

    public ACheckDelete(Orders<Order> orders) {
        super(orders);
    }

    @Override
    public void run() {
        synchronized (orders) {
            for (int i = orders.size() - 1; i >= 0; i--) {
                if (orders.get(i).status()) {
                    orders.remove(i);
                }
            }
        }
    }
}
