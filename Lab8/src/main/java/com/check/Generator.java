package com.check;

import com.demo.company.*;
import com.demo.company.Order;

/**
 * Генератор выбранного количества заказов
 */
public class Generator extends ACheck {
    private int count; //Количество заказов

    /**
     * Если количество заказов не выбрано, то происходит генерация 3х заказов
     * @param orders
     */
    public Generator(Orders<Order> orders) {
        super(orders);
        count = 3;
    }

    /**
     * Инициализация контейнера и количества
     * @param orders
     * @param count
     */
    public Generator(Orders<Order> orders, int count) {
        super(orders);
        this.count = count;
    }

    public Thread getThread() {
        return t;
    }

    public Order generate() {
        Device device = null;
        int randomNumber = (int) (Math.random() * 3);
        switch (randomNumber) {
            case 0:
                device = new Phone();
                break;
            case 1:
                device = new Smart();
                break;
            case 2:
                device = new Tablet();
                break;
            default:
                break;
        }
        device.create();
        ShoppingCart<Device> cart = new ShoppingCart<>();
        cart.add(device);
        Order order = new Order();
        order.checkout(cart);
        return order;
    }

    @Override
    public void run() {
        synchronized (orders) {
            for (int i = 0; i < count; i++) {
                orders.add(generate());
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
