package com.demo.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Scanner;
import java.util.UUID;

/**
 * Класс "Телефон", наследник от Device
 */
@Entity
public class Phone extends Device {
    private StringBuilder type_shell;

    /**
     * Конструктор без параметров
     * Инициализация полей
     */
    public Phone() {
        super();
        type_shell=new StringBuilder();
    }

    /**
     * Конструктор с парамертами (заполнение полей класса "Phone")
     * @param type_shell
     */
    public Phone(StringBuilder type_shell) {
        super();
        this.type_shell = type_shell;
    }

    /**
     * Конструктор с параметрами (заполение полей класса "Phone" и "Device")
     * @param deviceID
     * @param name
     * @param price
     * @param count
     * @param firm
     * @param model
     * @param os
     * @param type_shell
     */
    public Phone(UUID deviceID, StringBuilder name, int price, int count, StringBuilder firm, StringBuilder model, StringBuilder os, StringBuilder type_shell) {
        super(deviceID, name, price, count, firm, model, os);
        this.type_shell = type_shell;
    }

    /**
     * Геттеры и сеттеры
     * @return
     */
    public StringBuilder getType_shell() {
        return type_shell;
    }

    public void setType_shell(StringBuilder type_shell) {
        this.type_shell = type_shell;
    }

    /**
     * Заполнение полей случайным образом
     */
    @Override
    public void create() {
        super.create();
        String[] type_shel = {"Обычный", "Раскладушка"};
        int random_number = (int) (Math.random());

        type_shell.append(type_shel[random_number]);
    }

    /**
     * Вывод на экран
     */
    @Override
    public void read() {
        System.out.println("Type: Phone");
        super.read();
        System.out.print("Тип корпуса - " + type_shell + "\n");
    }

    /**
     * Заполнение полей вручную
     */
    @Override
    public void update() {
        type_shell.delete(0, type_shell.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Тип телефона: ");
        type_shell = new StringBuilder(scanner.nextLine());
    }

    /**
     * Удаление данных полей
     */
    @Override
    public void delete() {
        super.delete();
        type_shell.delete(0, type_shell.length());
    }
}
