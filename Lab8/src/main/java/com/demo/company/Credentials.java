package com.demo.company;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

/**
 * Класс "Заказчик"
 */
@Entity
public class Credentials implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID userID;
    private StringBuilder surname;
    private StringBuilder name;
    private StringBuilder sec_name;
    private StringBuilder email;

    /**
     * Конструктор без параметров
     * Присвоение ID
     * Инициализация полей
     */
    public Credentials() {
        userID = UUID.randomUUID();
        surname = new StringBuilder();
        name = new StringBuilder();
        sec_name = new StringBuilder();
        email = new StringBuilder();
    }

    /**
     * Консруктор с параметрами(заполнение всех полей)
     * @param userID
     * @param surname
     * @param name
     * @param sec_name
     * @param email
     */
    public Credentials(UUID userID, StringBuilder surname, StringBuilder name, StringBuilder sec_name, StringBuilder email) {
        this.userID = userID;
        this.surname = surname;
        this.name = name;
        this.sec_name = sec_name;
        this.email = email;
    }

    /**
     * Геттеры и сеттеры
     */
    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public StringBuilder getSurname() {
        return surname;
    }

    public void setSurname(StringBuilder surname) {
        this.surname = surname;
    }

    public StringBuilder getName() {
        return name;
    }

    public void setName(StringBuilder name) {
        this.name = name;
    }

    public StringBuilder getSec_name() {
        return sec_name;
    }

    public void setSec_name(StringBuilder sec_name) {
        this.sec_name = sec_name;
    }

    public StringBuilder getEmail() {
        return email;
    }

    public void setEmail(StringBuilder email) {
        this.email = email;
    }

    /**
     * Заполнение полей случайным образом
     */
    public void create() {
        surname.delete(0, surname.length());
        name.delete(0, name.length());
        sec_name.delete(0, sec_name.length());
        email.delete(0, email.length());
        String[] surnam = {"Иванов", "Петров", "Сидоров"};
        String[] nam = {"Иван", "Петр", "Сидор"};
        String[] sec_nam = {"Иванович", "Петрович", "Сидорович"};
        String[] emai = {"gmail", "mail", "yandex"};
        int random_number;
        random_number = (int) (Math.random() * 3);
        surname.append(surnam[random_number]);
        random_number = (int) (Math.random() * 3);
        name.append(nam[random_number]);
        random_number = (int) (Math.random() * 3);
        sec_name.append(sec_nam[random_number]);
        random_number = (int) (Math.random() * 3);
        email.append(emai[random_number]);
    }

    /**
     * Заполнение полей вручную с клавиатуры
     */
    public void update() {
        surname.delete(0, surname.length());
        name.delete(0, name.length());
        sec_name.delete(0, sec_name.length());
        email.delete(0, email.length());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Фамилия:");
        surname.append(scanner.nextLine());
        System.out.println("Имя:");
        name.append(scanner.nextLine());
        System.out.println("Отчество:");
        sec_name.append(scanner.nextLine());
        System.out.println("Email");
        email.append(scanner.nextLine());
    }

    /**
     * Вывод на экран
     */
    public void show() {
        System.out.print("\n\n" + userID + "\n" + surname + " " + name + " " + sec_name + "\n" + email + "\n\n");
    }
}
