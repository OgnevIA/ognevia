package com.demo.company;


import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Класс для хранения заказов
 * @param <T> - Шаблонный тип данных
 */
@Entity
@Component
public class Orders<T extends Order> implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @OneToMany(targetEntity = Order.class, cascade = {CascadeType.ALL})
    @JoinColumn(name = "orderID")
    private List<T> orders;

    /**
     * Конструктор без параметров
     * Инициализация списка заказов
     * Присвоение ID
     */
    public Orders() {
        orders = new ArrayList<>();
        id = UUID.randomUUID();
    }

    /**
     * Конструктор с 1м параметром
     * @param orders - Список для помещения в класс
     */
    public Orders(List<T> orders) {
        this.orders = orders;
        id = UUID.randomUUID();
    }

    /**
     * Конструктор с 2мя параметрами
     * @param id - Передаваемый ID
     * @param orders - Передаваемый контейнер
     */
    public Orders(UUID id, List<T> orders) {
        this.id = id;
        this.orders = orders;
    }

    /**
     * Геттеры и сеттеры
     */
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<T> getOrders() {
        return orders;
    }

    public void setOrders(List<T> orders) {
        this.orders = orders;
    }

    /**
     * Добавление заказа в коллекцию
     * @param order - Заказ для добавления
     */

    public void add(T order) {
        orders.add(order);
    }

    /**
     * Вывод на экран
     */
    public void show() {
        System.out.println("\n");
        if (orders.isEmpty()) {
            System.out.println("Список закаков пуст");
        } else {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).show();
            }
        }
    }

    /**
     * Возврат размера коллекции
     * @return
     */
    public int size() {
        return orders.size();
    }

    /**
     * Удаление заказа из коллекции по номеру
     * @param i - Номер для удаления
     */
    public void remove(int i) {
        orders.remove(i);
    }

    /**
     * Получить заказ по номеру
     * @param i - Номер заказа, который мы хотим получить
     * @return - Возврат найденного заказа
     */
    public T get(int i) {
        return orders.get(i);
    }

    /**
     * Очистить всю коллекцию
     */
    public void clear() {
        for (int i = orders.size() - 1; i >= 0; i--) {
            orders.remove(i);
        }
    }
}
