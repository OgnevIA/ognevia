package com.demo.company;

import java.io.Serializable;

/**
 * Интерфейс для Device и его наследников
 */
interface ICrudAction extends Serializable {
   public void create();
   public void read();
   public void update();
   public void delete();
}
