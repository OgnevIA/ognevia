package com.demo.company;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Класс "Корзина"
 * @param <T> - Ожидается, что в корзину будут складываться наследники от Device
 */
@Entity
public class ShoppingCart<T extends Device> implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID cartID;
    @ManyToMany(targetEntity = Device.class, cascade = {CascadeType.ALL})
    @JoinColumn(name = "deviceID")
    private List<T> devices;

    /**
     * Конструктор без параметров
     * Привоение ID
     * Инициализация контейнера
     */
    public ShoppingCart() {
        cartID = UUID.randomUUID();
        devices = new LinkedList<>();
    }

    /**
     * Конструктор с парамертами (заполнение всех полей)
     * @param cartID
     * @param devices
     */
    public ShoppingCart(UUID cartID, List<T> devices) {
        this.cartID = cartID;
        this.devices = devices;
    }

    /**
     * Геттеры и сеттеры
     * @return
     */
    public UUID getCartID() {
        return cartID;
    }

    public void setCartID(UUID cartID) {
        this.cartID = cartID;
    }

    public List<T> getDevices() {
        return devices;
    }

    public void setDevices(List<T> devices) {
        this.devices = devices;
    }

    /**
     * Добавление товара в корзину
     * @param element - Товар для добавления
     */
    public void add(T element) {
        devices.add(element);
    }

    /**
     * Удаление товара по номеру
     * @param count - Номер в коллекции для удаления
     */
    public void delete(int count) {
        devices.remove(count - 1);
    }

    /**
     * Вывод на экран
     */
    public void show() {
        System.out.println("\n");
        if (devices.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            for (int i = 0; i < devices.size(); i++) {
                devices.get(i).read();
                System.out.println("\n");
            }
            System.out.println("\n");
        }
    }
}
