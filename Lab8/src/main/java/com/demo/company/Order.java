package com.demo.company;

import com.sun.javafx.beans.IDProperty;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Класс "Заказ"
 */
@Entity
@Table(name = "ord")
public class Order implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID orderID;
    private StringBuilder status;
    private long time_of_create;
    private long time_of_wait;
    @ManyToOne(targetEntity = Credentials.class, cascade = {CascadeType.ALL})
    @JoinColumn(name = "userID")
    private Credentials user;
    @OneToOne(targetEntity = ShoppingCart.class, cascade = {CascadeType.ALL})
    @JoinColumn(name = "cartID")
    private ShoppingCart<Device> cart;

    /**
     * Конструктор без параметров
     * Статус - "В ожидании"
     * Задание случайным образом времени ожидания (От 1 до 4 секунд)
     * Присвоение ID
     */
    public Order() {
        status = new StringBuilder("В ожидании");
        time_of_wait = (1 + (int) (Math.random() * 4)) * 1000;
        user = new Credentials();
        cart = new ShoppingCart<>();
        orderID = UUID.randomUUID();
    }

    /**
     * Конструктор с параметрами(заполнение всех полей)
     * @param orderID
     * @param status
     * @param time_of_create
     * @param time_of_wait
     * @param user
     * @param cart
     */
    public Order(UUID orderID, StringBuilder status, long time_of_create, long time_of_wait, Credentials user, ShoppingCart<Device> cart) {
        this.status = status;
        this.time_of_create = time_of_create;
        this.time_of_wait = time_of_wait;
        this.user = user;
        this.cart = cart;
    }

    /**
     * Геттеры и сеттеры
     * @return
     */
    public UUID getOrderID() {
        return orderID;
    }

    public void setOrderID(UUID orderID) {
        this.orderID = orderID;
    }

    public StringBuilder getStatus() {
        return status;
    }

    public void setStatus(StringBuilder status) {
        this.status = status;
    }

    public long getTime_of_create() {
        return time_of_create;
    }

    public void setTime_of_create(long time_of_create) {
        this.time_of_create = time_of_create;
    }

    public long getTime_of_wait() {
        return time_of_wait;
    }

    public void setTime_of_wait(long time_of_wait) {
        this.time_of_wait = time_of_wait;
    }

    public Credentials getUser() {
        return user;
    }

    public void setUser(Credentials user) {
        this.user = user;
    }

    public ShoppingCart<Device> getCart() {
        return cart;
    }

    public void setCart(ShoppingCart<Device> cart) {
        this.cart = cart;
    }

    /**
     * Смена статуса на "Обработан"
     */
    public void changeStatus() {
        Date d = new Date(time_of_create + time_of_wait);
        if (d.equals(new Date()) || new Date().after(d)) {
            status = new StringBuilder("Обработан");
        }
    }

    /**
     * Вывод на экран
     */
    public void show() {
        System.out.println("\n");
        System.out.print(orderID + "\n" + status + " " + new Date(time_of_create) + " " + time_of_wait / 1000 + "\n");
        user.show();
        cart.show();
    }

    /**
     * Оформление заказа
     * @param cart - Корзина для оформления
     *             Создание данных заказчика
     */
    public void checkout(ShoppingCart cart) {
        user.create();
        this.cart = cart;
        time_of_create = new Date().getTime();
    }

    /**
     * Возврат статуса типом данных boolean
     * @return
     */
    public boolean status() {
        String a = "Обработан";
        if (status.toString().equals(a)) {
            return true;
        } else {
            return false;
        }
    }
}
