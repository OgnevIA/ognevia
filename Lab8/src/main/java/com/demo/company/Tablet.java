package com.demo.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Scanner;
import java.util.UUID;

/**
 * Класс "Планшет", наследник от Device
 */
@Entity
public class Tablet extends Device {
    private StringBuilder gpu;
    private int screen;

    /**
     * Консруктор без параметров
     * Инициализация полей
     */
    public Tablet() {
        super();
        gpu=new StringBuilder();
        screen = 10;
    }

    /**
     * Конструктор с параметрами (заполнение данных класса "Tablet")
     * @param gpu
     * @param screen
     */
    public Tablet(StringBuilder gpu, int screen) {
        super();
        this.gpu = gpu;
        this.screen = screen;
    }

    /**
     * Конструктор с параметрами (заполнение данных классов "Tablet" и "Device")
     * @param deviceID
     * @param name
     * @param price
     * @param count
     * @param firm
     * @param model
     * @param os
     * @param gpu
     * @param screen
     */
    public Tablet(UUID deviceID, StringBuilder name, int price, int count, StringBuilder firm, StringBuilder model, StringBuilder os, StringBuilder gpu, int screen) {
        super(deviceID, name, price, count, firm, model, os);
        this.gpu = gpu;
        this.screen = screen;
    }

    /**
     * Геттеры и сеттеры
     * @return
     */
    public StringBuilder getGpu() {
        return gpu;
    }

    public void setGpu(StringBuilder gpu) {
        this.gpu = gpu;
    }

    public int getScreen() {
        return screen;
    }

    public void setScreen(int screen) {
        this.screen = screen;
    }
    /**
     * Заполнение полей случайным образом
     */
    @Override
    public void create() {
        super.create();
        String[] gp = {"Intel", "Radeon", "NVidia"};
        int random_number = (int) (Math.random() * 3);
        gpu.append(gp[random_number]);
        random_number = (int) (Math.random() * 15);
        screen = random_number;
    }
    /**
     * Вывод на экран
     */
    @Override
    public void read() {
        System.out.println("Type: Tablet");
        super.read();
        System.out.print("Видеопроцессор - " + gpu + "\nРазрешение экрана - " + screen + "\n");
    }
    /**
     * Заполнение полей вручную
     */
    @Override
    public void update() {
        gpu.delete(0, gpu.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Видеопроцессор: ");
        gpu = new StringBuilder(scanner.nextLine());
        System.out.print("Разрешение экрана: ");
        screen = scanner.nextInt();
    }
    /**
     * Удаление данных полей
     */
    @Override
    public void delete() {
        super.delete();
        gpu.delete(0, gpu.length());
        screen = 0;
    }
}
