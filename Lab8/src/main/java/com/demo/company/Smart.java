package com.demo.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Scanner;
import java.util.UUID;

/**
 * Класс "Смартфон", наследник от Device
 */
@Entity
public class Smart extends Device {
    private StringBuilder type_sim;
    private int count_sim;

    /**
     * Конструктор без параметров
     * Инициализация полей
     */
    public Smart() {
        super();
        type_sim=new StringBuilder();
        count_sim = 1;
    }

    /**
     * Конструктор с параметрами (заполнение данных класса "Smart")
     * @param type_sim
     * @param count_sim
     */
    public Smart(StringBuilder type_sim, int count_sim) {
        super();
        this.type_sim = type_sim;
        this.count_sim = count_sim;
    }

    /**
     * Конструктор с параметрами (заполнение данных классов "Smart" и "Device")
     * @param deviceID
     * @param name
     * @param price
     * @param count
     * @param firm
     * @param model
     * @param os
     * @param type_sim
     * @param count_sim
     */
    public Smart(UUID deviceID, StringBuilder name, int price, int count, StringBuilder firm, StringBuilder model, StringBuilder os, StringBuilder type_sim, int count_sim) {
        super(deviceID, name, price, count, firm, model, os);
        this.type_sim = type_sim;
        this.count_sim = count_sim;
    }

    /**
     * Геттеры и сеттеры
     * @return
     */
    public StringBuilder getType_sim() {
        return type_sim;
    }

    public void setType_sim(StringBuilder type_sim) {
        this.type_sim = type_sim;
    }

    public int getCount_sim() {
        return count_sim;
    }

    public void setCount_sim(int count_sim) {
        this.count_sim = count_sim;
    }
    /**
     * Заполнение полей случайным образом
     */
    @Override
    public void create() {
        super.create();
        String[] typ_sim = {"Обычная SIM", "micro-SIM"};
        int random_number = (int) (Math.random());
        type_sim.append(typ_sim[random_number]);
        random_number = 1 + (int) (Math.random());
        count_sim = random_number;
    }
    /**
     * Вывод на экран
     */
    @Override
    public void read() {
        System.out.println("Type: SmartPhone");
        super.read();
        System.out.print("Тип sim-карт - " + type_sim + "\nКоличество sim-карт - " + count_sim + "\n");
    }
    /**
     * Заполнение полей вручную
     */
    @Override
    public void update() {
        type_sim.delete(0, type_sim.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Тип SIM-карт: ");
        type_sim = new StringBuilder(scanner.nextLine());
        System.out.print("Количество SIM-карт: ");
        count_sim = scanner.nextInt();
    }
    /**
     * Удаление данных полей
     */
    @Override
    public void delete() {
        super.delete();
        type_sim.delete(0, type_sim.length());
        count_sim = 0;
    }
}
