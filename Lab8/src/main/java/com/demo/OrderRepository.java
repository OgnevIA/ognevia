package com.demo;

import com.demo.company.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Репозиторий для хранения заказов
 */
public interface OrderRepository extends JpaRepository<Order, UUID> {

}
