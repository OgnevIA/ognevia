package com.demo;

import com.check.ACheck;
import com.check.Generator;
import com.demo.company.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

/**
 * Контроллер для работы с запросами
 */
@RestController
public class Web {
    /**
     * Сервис с реализованными методами
     */
    @Autowired
    private Service service;

    /**
     * Сохранение с БД count заказов
     * @param count - Сколько заказов сохранить
     * @return - Возврат "Ok" при удачной записи
     */
    @RequestMapping(value = "/save")
    public String save(@RequestParam int count) {
        service.save(count);
        return "Ok";
    }

    /**
     * Обработка запросов
     * @param command - Выбранная команда
     * @param order_id - ID заказа
     * @param cart_id - ID корзины
     * @return - Возврат результата выполнения метода из service
     */
    @RequestMapping(value = "/")
    public Object run(@RequestParam String command, @RequestParam(required = false) String order_id, @RequestParam(required = false) String cart_id) {
        if (command.equals("readall")) {
            return service.readall();
        } else if (command.equals("readById")) {
            return service.readById(order_id);
        } else if (command.equals("addToCart")) {
            return service.addToCart(cart_id);
        } else if (command.equals("delById")) {
            return service.delById(order_id);
        } else {
            return 3;
        }
    }
}

/**
 * Сервис, содержащий реализацию методов для Web
 */

@Component
class Service {
    /**
     * Репозиторий для заказрв
     */
    @Autowired
    private OrderRepository orderRepository;

    /**
     * Метод записи в БД
     * @param count - Сколько записать в БД
     *              Создание count случайных заказов и сохранение их в БД
     */
    public void save(int count) {
        Orders<Order> orders = new Orders<>();
        ACheck generator = new Generator(orders, count);
        try {
            generator.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < orders.size(); i++) {
            orderRepository.save(orders.get(i));
        }
    }

    /**
     * Чтение всех заказов из БД
     * @return - Возврат коллекции, содержащей все заказы
     */
    public List<Order> readall() {
        return orderRepository.findAll();
    }

    /**
     * Чтение из БД по ID
     * @param order_id - ID заказа, который необходимо найти
     * @return - Возврат найденного заказа
     */
    public Order readById(String order_id) {
        return orderRepository.findById(UUID.fromString(order_id)).get();
    }

    /**
     * Добавление случайного товара в корзину по ID корзины
     * @param cart_id - ID нужной корзины
     * @return - Возврат ID сгенерированного товара
     */
    public String addToCart(String cart_id) {
        Orders<Order> orders = new Orders<>(orderRepository.findAll());
        Device device = null;
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).getCart().getCartID().toString().equals(cart_id)) {
                int randomNumber = (int) (Math.random() * 3);
                switch (randomNumber) {
                    case 0:
                        device = new Phone();
                        break;
                    case 1:
                        device = new Smart();
                        break;
                    case 2:
                        device = new Tablet();
                        break;
                    default:
                        break;
                }
                device.create();
                orders.get(i).getCart().add(device);
                orderRepository.save(orders.get(i));
                break;
            }
        }
        return device.getDeviceID().toString();
    }

    /**
     * Удаление заказа по ID
     * @param order_id - ID заказа для удаления
     * @return - Возврат 0 при удачном удалении
     */
    public int delById(String order_id) {
        orderRepository.deleteById(UUID.fromString(order_id));
        return 0;
    }

}