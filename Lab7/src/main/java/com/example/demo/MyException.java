package com.example.demo;

public class MyException extends Exception {
    private int number;

    public MyException(String mes) {
        super(mes);
        if (mes.equals("Нет такого заказа")) {
            number = 1;
        }
        if (mes.equals("Файл поврежден")) {
            number = 2;
        }
        if (mes.equals("Неправильная команда")) {
            number = 3;
        }
    }

    public int getNumber() {
        return number;
    }
}
