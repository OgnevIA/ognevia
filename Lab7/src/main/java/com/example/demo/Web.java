package com.example.demo;


import com.check.ACheck;
import com.check.Generator;
import com.company.*;
import com.file.ManagerOrderJSON;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.log4j.Logger;

@RestController
@RequestMapping("/")
public class Web {

    private static final Logger logger = Logger.getLogger(Web.class);

    public Web() {
        Orders<Order> orders = new Orders<>();
        ACheck gen = new Generator(orders);
        try {
            gen.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ManagerOrderJSON json = new ManagerOrderJSON(orders);
        json.saveAll();
    }


    @RequestMapping
    public Object run(@RequestParam String command, @RequestParam(required = false) String order_id, @RequestParam(required = false) String cart_id) {
        if (command.equals("readall")) {
            return readall();
        } else if (command.equals("readById")) {
            return readById(order_id);
        } else if (command.equals("addToCart")) {
            return addToCard(cart_id);
        } else if (command.equals("delById")) {
            return delById(order_id);
        } else {
            try {
                throw new MyException("Неправильная команда");
            } catch (MyException e) {
                logger.error(command + " FALSE " + e.getNumber());
                return e.getNumber();
            }
        }
    }


    public Orders<Order> readall() {
        Orders<Order> orders = new Orders<>();
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            json.readAll();
            logger.info("READ ALL OK");
        } catch (Exception e) {
            logger.error("READ ALL FALSE");
        }
        return orders;
    }

    public Order readById(@RequestParam String order_id) {
        Orders<Order> orders = new Orders<>();
        Order order = null;
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            json.readById(order_id);
            order = orders.get(0);
            logger.info("READ BY ID " + order_id + " OK");
        } catch (Exception e) {
            logger.error("READ BY ID " + order_id + " FALSE");
        }
        return order;
    }

    public String addToCard(@RequestParam String cart_id) {
        Orders<Order> orders = new Orders<>();
        Device device = null;
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            json.readAll();
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getCart().getCardId().equals(cart_id)) {
                    int random_number = (int) (Math.random() * 3);
                    switch (random_number) {
                        case 0:
                            device = new Phone();
                            device.create();
                            break;
                        case 1:
                            device = new Smart();
                            device.create();
                            break;
                        case 2:
                            device = new Tablet();
                            device.create();
                            break;
                    }
                    orders.get(i).getCart().add(device);
                    break;
                }
            }
            json.saveAll();
            logger.info("ADD TO CART " + cart_id + " OK");
        } catch (Exception e) {
            logger.error("ADD TO CART " + cart_id + " FALSE");
        }
        return device.getDeviceID();
    }

    public int delById(@RequestParam String order_id) {
        try {
            Orders<Order> orders = new Orders<>();
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            if (json.available() == -1) {
                throw new MyException("Файл поврежден");
            }
            json.readAll();
            int n = orders.size();
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getOrderID().equals(order_id)) {
                    orders.remove(i);
                    break;
                }
            }
            if (n == orders.size()) {
                throw new MyException("Нет такого заказа");
            }
            json.saveAll();
        } catch (MyException e) {
            logger.error("DELETE BY ID " + order_id + " FALSE " + " " + e.getNumber());
            return e.getNumber();
        }
        logger.info("DELETE BY ID " + order_id + " OK");
        return 0;
    }
}
