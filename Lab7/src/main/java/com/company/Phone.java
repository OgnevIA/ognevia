package com.company;

import java.util.Scanner;

public class Phone extends Device {
    private StringBuilder type_shell = new StringBuilder();

    public Phone() {
        super();
        counter++;
    }

    public Phone(String type_shell) {
        super();
        counter++;
        this.type_shell.append(type_shell);
    }

    public StringBuilder getType_shell() {
        return type_shell;
    }

    public void setType_shell(StringBuilder type_shell) {
        this.type_shell = type_shell;
    }

    @Override
    public void create() {
        super.create();
        String[] type_shel = {"Обычный", "Раскладушка"};
        int random_number = (int) (Math.random());

        type_shell.append(type_shel[random_number]);
    }

    @Override
    public void read() {
        System.out.println("Type: Phone");
        super.read();
        System.out.print("Тип корпуса - " + type_shell + "\n");
    }

    @Override
    public void update() {
        type_shell.delete(0, type_shell.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Тип телефона: ");
        type_shell = new StringBuilder(scanner.nextLine());
    }

    @Override
    public void delete() {
        super.delete();
        type_shell.delete(0, type_shell.length());
    }
}
