package com.company;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;


public class Orders<T extends Order> implements Serializable {
    private volatile ArrayList<T> orders;
    private TreeMap<Long, T> treeMap;

    public Orders() {
        orders = new ArrayList<>();
        treeMap = new TreeMap<>();
    }

    public ArrayList<T> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<T> orders) {
        this.orders = orders;
    }

    public TreeMap<Long, T> getTreeMap() {
        return treeMap;
    }

    public void setTreeMap(TreeMap<Long, T> treeMap) {
        this.treeMap = treeMap;
    }

    public void add(T order) {
        orders.add(order);
        treeMap.put(order.getTime_of_create(), order);
    }

    public void show() {
        System.out.println("\n");
        if (orders.isEmpty()) {
            System.out.println("Список закаков пуст");
        } else {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).show();
            }
        }
    }

    public int size() {
        return orders.size();
    }

    public void remove(int i) {
        treeMap.remove(orders.get(i).getTime_of_create());
        orders.remove(i);
    }

    public T get(int i) {
        return orders.get(i);
    }

    public void delete() {
        for (int i = orders.size() - 1; i >= 0; i--) {
            orders.remove(i);
        }
    }
}
