package com.file;

import com.company.Order;
import com.company.Orders;

public abstract class AManagerOrder implements IOrder {
    protected Orders<Order> orders;

    public AManagerOrder() {
        orders = new Orders<>();
    }

    public AManagerOrder(Orders<Order> orders) {
        this.orders = orders;
    }

    public Orders<Order> getOrders() {
        return orders;
    }

    public void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

    @Override
    public abstract void readById(String id);

    @Override
    public abstract void saveById(String id);

    @Override
    public abstract void readAll();

    @Override
    public abstract void saveAll();
}
