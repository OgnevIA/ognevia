package com.check;

import com.company.*;

public class Generator extends ACheck {
    private int count; //Количество заказов

    public Generator() {
        super();
    }

    public Generator(Orders<Order> orders) {
        super(orders);
    }

    public Thread getThread() {
        return t;
    }

    public Order generate() {
        ShoppingCart<Device> cart = new ShoppingCart<>();
        Order order = new Order();
        Device phone = new Phone();
        Device smart = new Smart();
        Device tablet = new Tablet();
        int ch = (int) (Math.random() * 3);
        switch (ch) {
            case 0:
                phone.create();
                cart.add(phone);
                break;
            case 1:
                smart.create();
                cart.add(smart);
                break;
            case 2:
                tablet.create();
                cart.add(tablet);
                break;
            default:
                break;
        }
        order.checkout(cart);
        return order;
    }

    @Override
    public void run() {
        count = 1;
        synchronized (orders) {
            for (int i = 0; i < count; i++) {
                orders.add(generate());
            }
        }
    }
}
