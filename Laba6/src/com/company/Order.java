package com.company;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


public class Order implements Serializable {
    private String orderID;
    private StringBuilder status;
    private long time_of_create;
    private long time_of_wait;
    private Credentials man;
    private ShoppingCart cart;

    public Order() {
        status = new StringBuilder("В ожидании");
        time_of_wait = (1 + (int) (Math.random() * 4)) * 1000;
        man = new Credentials();
        cart = new ShoppingCart();
        orderID = UUID.randomUUID().toString();
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public StringBuilder getStatus() {
        return status;
    }

    public void setStatus(StringBuilder status) {
        this.status = status;
    }

    public long getTime_of_create() {
        return time_of_create;
    }

    public void setTime_of_create(long time_of_create) {
        this.time_of_create = time_of_create;
    }

    public long getTime_of_wait() {
        return time_of_wait;
    }

    public void setTime_of_wait(long time_of_wait) {
        this.time_of_wait = time_of_wait;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public Credentials getMan() {
        return man;
    }

    public void setMan(Credentials man) {
        this.man = man;
    }

    public void changeStatus() {
        Date d = new Date(time_of_create + time_of_wait);
        if (d.equals(new Date()) || new Date().after(d)) {
            status = new StringBuilder("Обработан");
        }
    }

    public void show() {
        System.out.println("\n");
        System.out.print(orderID + "\n" + status + " " + new Date(time_of_create) + " " + time_of_wait / 1000 + "\n");
        man.show();
        cart.show();
    }

    public void checkout(ShoppingCart cart) {
        man.create();
        this.cart = cart;
        time_of_create = new Date().getTime();
    }

    public boolean status() {
        String a = "Обработан";
        if (status.toString().equals(a)) {
            return true;
        } else {
            return false;
        }
    }
}
