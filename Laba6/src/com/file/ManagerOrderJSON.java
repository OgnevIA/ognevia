package com.file;


import com.company.Order;
import com.company.Orders;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;


public class ManagerOrderJSON extends AManagerOrder {
    private String name = "/home/igor/IdeaProjects/Lab6/json.json";

    public ManagerOrderJSON() {
        super();
    }

    public ManagerOrderJSON(Orders<Order> orders) {
        super(orders);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override

    public void readById(String id) {
        try (FileReader reader = new FileReader(name)) {
            ObjectMapper mapper = new ObjectMapper();
            Orders<Order> orderOrders = (Orders<Order>) mapper.readValue(reader, Orders.class);
            boolean mark = false;
            for (int i = 0; i < orderOrders.size(); i++) {
                if (orderOrders.get(i).getOrderID().equals(id)) {
                    mark = true;
                    orders.add(orderOrders.get(i));
                    break;
                }
            }
            if (!mark) {
                System.out.println("Нет такого ID");
            }
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveById(String id) {
        try (FileWriter writer = new FileWriter(name)) {
            Orders<Order> orderOrders = new Orders<>();
            boolean mark = false;
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getOrderID().equals(id)) {
                    mark = true;
                    orderOrders.add(orders.get(i));
                    break;
                }
            }
            if (mark) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.writerWithDefaultPrettyPrinter().writeValue(writer, orderOrders);
            } else {
                System.out.println("Нет такого ID");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void readAll() {
        try (FileReader reader = new FileReader(name)) {
            ObjectMapper mapper = new ObjectMapper();
            Orders<Order> orderOrders = (Orders<Order>) mapper.readValue(reader, Orders.class);
            orders.setOrders(orderOrders.getOrders());
            orders.setTreeMap(orderOrders.getTreeMap());
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAll() {
        try (FileWriter writer = new FileWriter(name)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, orders);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
