package com.file;

import com.company.Order;
import com.company.Orders;

import java.io.*;

public class ManagerOrderFile extends AManagerOrder {
    private String name = "/home/igor/IdeaProjects/Lab5/bin.dat";

    public ManagerOrderFile() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ManagerOrderFile(Orders<Order> orders) {
        super(orders);
    }

    @Override
    public void readById(String id) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(name))) {
            Order order = null;
            boolean mark = false;
            try {
                while (true) {
                    order = new Order();
                    order = (Order) inputStream.readObject();
                    if (order.getOrderID().equals(id)) {
                        mark = true;
                        orders.add(order);
                        break;
                    }
                }
            } catch (EOFException e){
                System.out.println("Successfully");
            }
            if (!mark) {
                System.out.println("Нет такого ID");
            }


        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveById(String id) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(name))) {
            boolean mark = false;
            int count = -1;
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getOrderID().equals(id)) {
                    mark = true;
                    count = i;
                    break;
                }
            }
            if (mark) {
                outputStream.writeObject(orders.get(count));
            } else {
                System.out.println("Нет такого ID");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void readAll() {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(name))) {
            Order order = new Order();
            while (true) {
                order = (Order) inputStream.readObject();
                orders.add(order);
                order = new Order();
            }

        } catch (EOFException e) {
            System.out.println("Successfully");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAll() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(name))) {
            for (int i = 0; i < orders.size(); i++) {
                outputStream.writeObject(orders.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
