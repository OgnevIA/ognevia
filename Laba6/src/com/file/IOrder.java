package com.file;

interface IOrder {
    public void readById(String id);
    public void saveById(String id);
    public void readAll();
    public void saveAll();
}
