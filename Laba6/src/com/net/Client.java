package com.net;

import com.check.Generator;
import com.company.Order;
import com.company.Orders;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class Client {
    private InetAddress host;
    private int portTCP;
    private int portUDP;
    private Order order;

    public Client() {
        portUDP = 4444;
        order = new Order();
    }

    public Client(int portUDP) {
        this.portUDP = portUDP;
    }

    public void getPort() {
        try {
            System.out.println("Listening Port");
            while (true) {
                DatagramPacket packet = new DatagramPacket(new byte[4], 4);
                DatagramSocket socket = new DatagramSocket(portUDP);
                socket.receive(packet);
                byte[] data = packet.getData();
                portTCP = ByteBuffer.wrap(data).getInt();
                host = packet.getAddress();
                socket.close();
                if (portTCP >= 1024) {
                    break;
                }
                System.out.println("Port received");
            }
            TimeUnit.SECONDS.sleep(2);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sentData() {
        System.out.println("Start connection");
        try (Socket client = new Socket(host, portTCP)) {
            System.out.println("Connected to server");
            Orders<Order> orders = new Orders<>();
            Generator generator = new Generator(orders);
            try {
                generator.getThread().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order = orders.get(0);
            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            out.writeInt(portUDP);
            out.writeObject(order);
            TimeUnit.SECONDS.sleep(2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }

    public void getMessage() {
        try {
            System.out.println("Listening Answer");
            while (true) {
                DatagramPacket packet = new DatagramPacket(new byte[4], 4);
                DatagramSocket socket = new DatagramSocket(portUDP);
                socket.receive(packet);
                byte[] data = packet.getData();
                int status = ByteBuffer.wrap(data).getInt();
                socket.close();
                if (1 == status) {
                    order.setStatus(new StringBuilder("Обработан"));
                    order.show();
                    break;
                }
            }
            TimeUnit.SECONDS.sleep(2);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Client client = new Client(4444);
        while (true) {
            client.getPort();
            client.sentData();
            client.getMessage();
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
