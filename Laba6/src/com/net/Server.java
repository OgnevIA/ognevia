package com.net;

import com.check.ACheck;
import com.check.ACheckDelete;
import com.check.ACheckStatus;
import com.company.Order;
import com.company.Orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Server {
    private volatile InetAddress host;
    private volatile int portTCP;
    private volatile Orders<Order> orders;
    private List<Integer> ports;
    private int portUDPstart;
    private int portUDPend;

    public Server() throws UnknownHostException {
        host = InetAddress.getByName("localhost");
        portTCP = 5610;
        portUDPstart = 4440;
        portUDPend = 4450;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public Server(InetAddress host) {
        this.host = host;
        portTCP = 5610;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public Server(int portTCP) throws UnknownHostException {
        this.portTCP = portTCP;
        host = InetAddress.getByName("localhost");
        portUDPstart = 4440;
        portUDPend = 4450;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public Server(InetAddress host, int portTCP) {
        this.host = host;
        this.portTCP = portTCP;
        portUDPstart = 4440;
        portUDPend = 4450;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public Server(int portUDPstart, int portUDPend) throws UnknownHostException {
        host = InetAddress.getByName("localhost");
        portTCP = 5610;
        this.portUDPstart = portUDPstart;
        this.portUDPend = portUDPend;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public Server(InetAddress host, int portTCP, int portUDPstart, int portUDPend) {
        this.host = host;
        this.portTCP = portTCP;
        this.portUDPstart = portUDPstart;
        this.portUDPend = portUDPend;
        orders = new Orders<>();
        ports = new ArrayList<>();
    }

    public void sendPort() {
        while (true) {
            try {
                byte[] data = ByteBuffer.allocate(4).putInt(portTCP).array();
                for (int i = portUDPstart; i < portUDPend; i++) {
                    DatagramPacket packet = new DatagramPacket(data, data.length, host, i);
                    DatagramSocket socket = new DatagramSocket();
                    socket.send(packet);
                    socket.close();
                }
                System.out.println("Port sent");
                TimeUnit.SECONDS.sleep(5);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void getData() {
        while (true) {
            System.out.println("Start connection");
            try (ServerSocket server = new ServerSocket(portTCP)) {
                System.out.println("Waiting for a client...");
                Socket client = server.accept();
                System.out.println("Client is connected");
                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                int port = in.readInt();
                ports.add(port);
                Order order;
                order = (Order) in.readObject();
                in.close();
                client.close();
                orders.add(order);
                TimeUnit.SECONDS.sleep(2);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Done");
        }
    }

    public void processing() {
        while (true) {
            ACheck status;
            ACheck delete;
            status = new ACheckStatus(orders);
            try {
                status.getThread().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).status()) {
                    sentStatus(i);
                }
            }
            delete = new ACheckDelete(orders);
            try {
                delete.getThread().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void sentStatus(int port) {
        try {
            TimeUnit.SECONDS.sleep(2);
            int status = 1;
            byte[] data = ByteBuffer.allocate(4).putInt(status).array();
            DatagramPacket packet = new DatagramPacket(data, data.length, host, ports.get(port));
            DatagramSocket socket = new DatagramSocket();
            socket.send(packet);
            socket.close();
            ports.remove(port);
            System.out.println("Answer sent");
            TimeUnit.SECONDS.sleep(2);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        Thread threadPort = new Thread(this::sendPort);
        threadPort.start();
        Thread threadData = new Thread(this::getData);
        threadData.start();
        Thread threadProcessing = new Thread(this::processing);
        threadProcessing.start();
    }

    public static void main(String[] args) throws UnknownHostException {
        Server server = new Server();
        server.run();
    }
}
