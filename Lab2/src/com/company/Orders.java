package com.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

public class Orders {
    ArrayList<Order> orders;
    TreeMap<Date, Order> treeMap;

    Orders() {
        orders = new ArrayList<>();
        treeMap = new TreeMap<>();
    }

    public void add(Order order) {
        orders.add(order);
        treeMap.put(order.getTime_of_create(), order);
    }

    public void show() {
        System.out.println("\n");
        if (orders.isEmpty()) {
            System.out.println("Список закаков пуст");
        } else {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).show();
            }
        }
    }

    public void review() {
        String a = "Обработан";
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).getStatus().toString().equals(a)) {
                orders.remove(i);
            }
        }
    }
}
