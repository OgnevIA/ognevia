package com.company;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;

public class ShoppingCart {
    LinkedList<Device> cart;
    HashSet<String> hashSet;

    ShoppingCart() {
        cart = new LinkedList<>();
        hashSet = new HashSet();
    }

    public void add(Device element) {
        cart.add(element);
        hashSet.add(element.getId());
    }

    public void delete(int count) {
        hashSet.remove(cart.get(count - 1).getId());
        cart.remove(count - 1);
    }

    public void show() {
        System.out.println("\n");
        if (cart.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            for (int i = 0; i < cart.size(); i++) {
                cart.get(i).read();
                System.out.println("\n");
            }
            System.out.println("\n");
        }
    }

    public LinkedList get() {
        return cart;
    }

    public void search(String id) {
        if (hashSet.contains(id)) {
            System.out.println("Товар найден");
        } else {
            System.out.println("Товар не найден");
        }
    }
}
