package com.company;

import java.util.Scanner;

public class Tablet extends Device {
    StringBuilder gpu = new StringBuilder();
    int screen;

    public Tablet() {
        super();
        counter = counter + 1;
        screen = 10;
    }

    public Tablet(String gpu) {
        super();
        counter = counter + 1;
        screen = 10;
        this.gpu.append(gpu);
    }

    public Tablet(StringBuilder gpu, int screen) {
        super();
        counter = counter + 1;
        this.screen = screen;
        this.gpu.append(gpu);
    }

    @Override
    public void create() {
        super.create();
        String[] gp = {"Intel", "Radeon", "NVidia"};
        int random_number = (int) (Math.random() * 3);
        gpu.append(gp[random_number]);
        random_number = (int) (Math.random() * 15);
        screen = random_number;
    }

    @Override
    public void read() {
        System.out.println("Type: Tablet");
        super.read();
        System.out.print("Видеопроцессор - " + gpu + "\nРазрешение экрана - " + screen + "\n");
    }

    @Override
    public void update() {
        gpu.delete(0, gpu.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Видеопроцессор: ");
        gpu = new StringBuilder(scanner.nextLine());
        System.out.print("Разрешение экрана: ");
        screen = scanner.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        gpu.delete(0, gpu.length());
        screen = 0;
    }
}
