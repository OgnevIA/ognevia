package com.company;


import com.check.Generator;
import com.file.ManagerOrderFile;
import com.file.ManagerOrderJSON;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Orders<Order> orders = new Orders<>();
        Generator generator = new Generator(orders);
        try {
            generator.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        orders.show();
        System.out.println("-----------BIN");
        ManagerOrderFile file=new ManagerOrderFile(orders);
        file.saveAll();
        orders.delete();
        file.readAll();
        orders.show();
        System.out.println("-----------JSON");
        ManagerOrderJSON json=new ManagerOrderJSON(orders);
        json.saveAll();
        orders.delete();
        json.readAll();
        orders.show();
        System.out.println("-----------BINIDSave");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите id для сохранения");
        String id=scanner.next();
        file.saveById(id);
        orders.delete();
        file.readAll();
        orders.show();
        System.out.println("-----------JSONIDSave");
        json.saveById(id);
        orders.delete();
        json.readAll();
        orders.show();
        System.out.println("-----------BINIDRead");
        generator = new Generator(orders);
        try {
            generator.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        orders.show();
        System.out.println("Введите id для чтения");
        id=scanner.next();
        file.saveAll();
        orders.delete();
        file.readById(id);
        orders.show();
        System.out.println("-----------JSONIDRead");
        generator = new Generator(orders);
        try {
            generator.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        orders.show();
        System.out.println("Введите id для чтения");
        id=scanner.next();
        json.saveAll();
        orders.delete();
        json.readById(id);
        orders.show();
    }
}
