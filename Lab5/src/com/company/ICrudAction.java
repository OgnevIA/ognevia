package com.company;

import java.io.Serializable;

interface ICrudAction extends Serializable {
   public void create();
   public void read();
   public void update();
   public void delete();
}
