package com.company;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;

public class ShoppingCart<T extends Device> implements Serializable {
    private HashSet<String> hashSet;
    private LinkedList<T> cart;


    public ShoppingCart() {
        cart = new LinkedList<>();
        hashSet = new HashSet();
    }

    public LinkedList<T> getCart() {
        return cart;
    }

    public void setCart(LinkedList<T> cart) {
        this.cart = cart;
    }

    public HashSet<String> getHashSet() {
        return hashSet;
    }

    public void setHashSet(HashSet<String> hashSet) {
        this.hashSet = hashSet;
    }

    public void add(T element) {
        cart.add(element);
        hashSet.add(element.getDeviceID());
    }

    public void delete(int count) {
        hashSet.remove(cart.get(count - 1).getDeviceID());
        cart.remove(count - 1);
    }

    public void show() {
        System.out.println("\n");
        if (cart.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            for (int i = 0; i < cart.size(); i++) {
                cart.get(i).read();
                System.out.println("\n");
            }
            System.out.println("\n");
        }
    }

    public LinkedList get() {
        return cart;
    }

    public void search(String id) {
        if (hashSet.contains(id)) {
            System.out.println("Товар найден");
        } else {
            System.out.println("Товар не найден");
        }
    }
}
