package com.company;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.Scanner;
import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Phone.class, name = "PHONE"),
        @JsonSubTypes.Type(value = Smart.class, name = "SMART"),
        @JsonSubTypes.Type(value = Tablet.class, name = "TABLET")
})
public abstract class Device implements ICrudAction {
    private String deviceID;
    private StringBuilder name = new StringBuilder();
    private int price;
    private int count;
    private StringBuilder firm = new StringBuilder();
    private StringBuilder model = new StringBuilder();
    private StringBuilder os = new StringBuilder();
    public static int counter = 0;

    public Device() {
        super();
        counter++;
        deviceID = UUID.randomUUID().toString();
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public StringBuilder getName() {
        return name;
    }

    public void setName(StringBuilder name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public StringBuilder getFirm() {
        return firm;
    }

    public void setFirm(StringBuilder firm) {
        this.firm = firm;
    }

    public StringBuilder getModel() {
        return model;
    }

    public void setModel(StringBuilder model) {
        this.model = model;
    }

    public StringBuilder getOs() {
        return os;
    }

    public void setOs(StringBuilder os) {
        this.os = os;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Device.counter = counter;
    }

    @Override
    public void create() {
        String name[] = {"Samsumg", "Nokia", "Sony", "LG", "IPhone"};
        String firm[] = {"Samsumg", "Nokia", "Sony", "LG", "Apple"};
        String model[] = {"A3 2017", "Xperia", "G4", "Asha", "X"};
        String os[] = {"Windows", "Android", "IOs"};
        int random_number;
        random_number = (int) (Math.random() * 4);
        setName(new StringBuilder().append(name[random_number]));
        random_number = (int) (Math.random() * 4);
        setFirm(new StringBuilder(firm[random_number]));
        random_number = (int) (Math.random() * 4);
        setModel(new StringBuilder(model[random_number]));
        random_number = (int) (Math.random() * 2);
        setOs(new StringBuilder(os[random_number]));
        random_number = (int) (Math.random() * 100000);
        setPrice(random_number);
        random_number = (int) (Math.random() * 1000);
        setCount(random_number);
    }

    @Override
    public void read() {
        System.out.print("ID: " + deviceID + "\nНаименование - " + name + "\nЦена - " + price + "\nКоличество - " + count + "\nФирма - " + firm + "\nМодель - " + model + "\nОС - " + os + "\n");
    }

    @Override
    public void update() {
        name.delete(0, name.length());
        firm.delete(0, firm.length());
        model.delete(0, model.length());
        os.delete(0, os.length());
        Scanner scanner = new Scanner(System.in);
        int temp;
        String tmp;
        System.out.print("Наименование: ");
        tmp = scanner.nextLine();
        setName(new StringBuilder(tmp));
        System.out.print("Цена: ");
        temp = scanner.nextInt();
        setPrice(temp);
        System.out.print("Количество: ");
        temp = scanner.nextInt();
        setCount(temp);
        String empty = scanner.nextLine();
        System.out.print("Фирма: ");
        tmp = scanner.nextLine();
        setFirm(new StringBuilder(tmp));
        System.out.print("Модель: ");
        tmp = scanner.nextLine();
        setModel(new StringBuilder(tmp));
        System.out.print("ОС: ");
        tmp = scanner.nextLine();
        setOs(new StringBuilder(tmp));
    }

    @Override
    public void delete() {
        counter--;
        name.delete(0, name.length());
        firm.delete(0, firm.length());
        model.delete(0, model.length());
        os.delete(0, os.length());
        price = 0;
        count = 0;
    }
}
