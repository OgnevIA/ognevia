package com.check;

import com.company.Order;
import com.company.Orders;

public abstract class ACheck implements Runnable {
    protected Orders<Order> orders;
    protected Thread t;

    public ACheck() {
        orders = new Orders<>();
        t = new Thread(this);
        t.start();
    }

    public ACheck(Orders<Order> orders) {
        this.orders = orders;
        t = new Thread(this);
        t.start();
    }

    public Thread getThread() {
        return t;
    }
}
