package com.check;

import com.company.Order;
import com.company.Orders;

import java.util.concurrent.TimeUnit;

public class ACheckStatus extends ACheck {

    public ACheckStatus() {
        super();
    }

    public ACheckStatus(Orders<Order> orders) {
        super(orders);
    }

    @Override
    public void run() {
        synchronized (orders) {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).changeStatus();
            }
        }
    }
}
