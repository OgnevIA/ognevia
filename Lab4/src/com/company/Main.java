package com.company;

import com.check.ACheck;
import com.check.ACheckDelete;
import com.check.ACheckStatus;
import com.check.Generator;

import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        Orders<Order> orders = new Orders<>();
        ACheck generator;
        ACheck checkStatus;
        ACheck checkDelete;
        int random_number;
        for (int i = 0; i < 5; i++) {
            generator = new Generator(orders);
            checkStatus = new ACheckStatus(orders);
            checkDelete = new ACheckDelete(orders);
            try {
                generator.getThread().join();
                checkStatus.getThread().join();
                checkDelete.getThread().join();
            } catch (InterruptedException r) {
                r.printStackTrace();
            }
            orders.show();
            random_number = 1 + (int) (Math.random() * 4);
            try {
                TimeUnit.SECONDS.sleep(random_number);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("---------------------");
        }
    }
}
