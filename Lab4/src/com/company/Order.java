package com.company;

import java.util.Date;
import java.util.Scanner;

public class Order {
    private StringBuilder status;
    private Date time_of_create;
    private long time_of_wait;
    private ShoppingCart cart;
    private Credentials man;

    public Order() {
        status = new StringBuilder("В ожидании");
        time_of_wait = (1 + (int) (Math.random() * 4)) * 1000;
        man = new Credentials();
    }

    public Date getTime_of_create() {
        return time_of_create;
    }

    public long getTime_of_wait() {
        return time_of_wait;
    }

    public StringBuilder getStatus() {
        return status;
    }

    public void changeStatus() {
        Date d = new Date(time_of_create.getTime() + time_of_wait);
        if (d.equals(new Date()) || new Date().after(d)) {
            status = new StringBuilder("Обработан");
        }
    }

    public void show() {
        System.out.println("\n");
        man.show();
        System.out.print(status + " " + time_of_create + " " + time_of_wait / 1000 + "\n");
        cart.show();
    }

    public void checkout(ShoppingCart cart) {
        this.cart = cart;
        man.create();
        time_of_create = new Date();
    }

    public boolean status() {
        String a = "Обработан";
        if (status.toString().equals(a)) {
            return true;
        } else {
            return false;
        }
    }
}
