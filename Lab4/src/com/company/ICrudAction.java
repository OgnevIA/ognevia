package com.company;

interface ICrudAction {
   public void create();
   public void read();
   public void update();
   public void delete();
}
