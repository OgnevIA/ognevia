package com.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

public class Orders<T extends Order> {
    private volatile ArrayList<T> orders;
    private TreeMap<Date, T> treeMap;

    public Orders() {
        orders = new ArrayList<>();
        treeMap = new TreeMap<>();
    }

    public void add(T order) {
        orders.add(order);
        treeMap.put(order.getTime_of_create(), order);
    }

    public void show() {
        System.out.println("\n");
        if (orders.isEmpty()) {
            System.out.println("Список закаков пуст");
        } else {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).show();
            }
        }
    }

    public int size() {
        return orders.size();
    }

    public void remove(int i) {
        orders.remove(i);
    }

    public T get(int i) {
        return orders.get(i);
    }
}
