package com.company;

import java.util.Scanner;
import java.util.UUID;

public abstract class Device implements ICrudAction {
    private String id;
    private StringBuilder name=new StringBuilder();
    private int price;
    private int count;
    private StringBuilder firm=new StringBuilder();
    private StringBuilder model=new StringBuilder();
    private StringBuilder os=new StringBuilder();
    public static int counter=0;
    public Device(){
        super();
        counter++;
        id = UUID.randomUUID().toString();
    }
    public void setName(String name){
        this.name.append(name);
    }
    public StringBuilder getName(){
        return name;
    }
    public void setPrice(int price){
        this.price=price;
    }
    public int getPrice(){
        return price;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setFirm(String firm) {
        this.firm.append(firm);
    }

    public StringBuilder getFirm() {
        return firm;
    }

    public void setModel(String model) {
        this.model.append(model);
    }

    public StringBuilder getModel() {
        return model;
    }

    public void setOs(String os) {
        this.os.append(os);
    }

    public StringBuilder getOs() {
        return os;
    }

    public String getId() {
        return id;
    }

    @Override
    public void create(){
        String name[] = {"Samsumg", "Nokia", "Sony", "LG", "IPhone"};
        String firm[] = {"Samsumg", "Nokia", "Sony", "LG", "Apple"};
        String model[] = {"A3 2017", "Xperia", "G4", "Asha", "X"};
        String os[] = {"Windows", "Android", "IOs"};
        int random_number;
        random_number = (int) (Math.random() * 4);
        setName(name[random_number]);
        random_number = (int) (Math.random() * 4);
        setFirm(firm[random_number]);
        random_number = (int) (Math.random() * 4);
        setModel(model[random_number]);
        random_number = (int) (Math.random() * 2);
        setOs(os[random_number]);
        random_number = (int) (Math.random() * 100000);
        setPrice(random_number);
        random_number = (int) (Math.random() * 1000);
        setCount(random_number);
    }

    @Override
    public void read(){
        System.out.print("ID: "+id+"\nНаименование - "+name+"\nЦена - "+price+"\nКоличество - "+count+"\nФирма - "+firm+"\nМодель - "+model + "\nОС - "+os+"\n");
    }

    @Override
    public void update(){
        name.delete(0, name.length());
        firm.delete(0, firm.length());
        model.delete(0,model.length());
        os.delete(0,os.length());
        Scanner scanner = new Scanner(System.in);
        int temp;
        String tmp;
        System.out.print("Наименование: ");
        tmp = scanner.nextLine();
        setName(tmp);
        System.out.print("Цена: ");
        temp = scanner.nextInt();
        setPrice(temp);
        System.out.print("Количество: ");
        temp = scanner.nextInt();
        setCount(temp);
        String empty =scanner.nextLine();
        System.out.print("Фирма: ");
        tmp = scanner.nextLine();
        setFirm(tmp);
        System.out.print("Модель: ");
        tmp = scanner.nextLine();
        setModel(tmp);
        System.out.print("ОС: ");
        tmp = scanner.nextLine();
        setOs(tmp);
    }

    @Override
    public void delete(){
        counter--;
        name.delete(0, name.length());
        firm.delete(0, firm.length());
        model.delete(0,model.length());
        os.delete(0,os.length());
        price=0;
        count=0;
    }
}
