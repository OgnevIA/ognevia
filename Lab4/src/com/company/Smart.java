package com.company;

import java.util.Scanner;

public class Smart extends Device {
    private StringBuilder type_sim = new StringBuilder();
    private int count_sim;

    public Smart() {
        super();
        counter++;
        count_sim = 1;
    }

    public Smart(StringBuilder type_sim) {
        super();
        counter++;
        this.type_sim = type_sim;
        count_sim = 1;
    }

    public Smart(StringBuilder type_sim, int count_sim) {
        super();
        counter++;
        this.type_sim = type_sim;
        this.count_sim = count_sim;
    }

    @Override
    public void create() {
        super.create();
        String[] typ_sim = {"Обычная SIM", "micro-SIM"};
        int random_number = (int) (Math.random());
        type_sim.append(typ_sim[random_number]);
        random_number = 1 + (int) (Math.random());
        count_sim = random_number;
    }

    @Override
    public void read() {
        System.out.println("Type: SmartPhone");
        super.read();
        System.out.print("Тип sim-карт - " + type_sim + "\nКоличество sim-карт - " + count_sim + "\n");
    }

    @Override
    public void update() {
        type_sim.delete(0, type_sim.length());
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Тип SIM-карт: ");
        type_sim = new StringBuilder(scanner.nextLine());
        System.out.print("Количество SIM-карт: ");
        count_sim = scanner.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        type_sim.delete(0, type_sim.length());
        count_sim = 0;
    }
}
