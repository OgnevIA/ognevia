package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Credentials {
    String id;
    StringBuilder surname;
    StringBuilder name;
    StringBuilder sec_name;
    StringBuilder email;

    public Credentials() {
        id = UUID.randomUUID().toString();
        surname = new StringBuilder();
        name = new StringBuilder();
        sec_name = new StringBuilder();
        email = new StringBuilder();
    }

    public Credentials(String surname, String name, String sec_name, String email) {
        id = UUID.randomUUID().toString();
        this.surname = new StringBuilder();
        this.name = new StringBuilder();
        this.sec_name = new StringBuilder();
        this.email = new StringBuilder();
        this.surname.append(surname);
        this.name.append(name);
        this.sec_name.append(sec_name);
        this.email.append(email);
    }

    public void create() {
        surname.delete(0, surname.length());
        name.delete(0, name.length());
        sec_name.delete(0, sec_name.length());
        email.delete(0, email.length());
        String[] surnam = {"Иванов", "Петров", "Сидоров"};
        String[] nam = {"Иван", "Петр", "Сидор"};
        String[] sec_nam = {"Иванович", "Петрович", "Сидорович"};
        String[] emai = {"gmail", "mail", "yandex"};
        int random_number;
        random_number = (int) (Math.random() * 3);
        surname.append(surnam[random_number]);
        random_number = (int) (Math.random() * 3);
        name.append(nam[random_number]);
        random_number = (int) (Math.random() * 3);
        sec_name.append(sec_nam[random_number]);
        random_number = (int) (Math.random() * 3);
        email.append(emai[random_number]);
    }

    public void update() {
        surname.delete(0, surname.length());
        name.delete(0, name.length());
        sec_name.delete(0, sec_name.length());
        email.delete(0, email.length());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Фамилия:");
        surname.append(scanner.nextLine());
        System.out.println("Имя:");
        name.append(scanner.nextLine());
        System.out.println("Отчество:");
        sec_name.append(scanner.nextLine());
        System.out.println("Email");
        email.append(scanner.nextLine());
    }

    public void show() {
        System.out.print("\n\n" + id + "\n" + surname + " " + name + " " + sec_name + "\n" + email + "\n\n");
    }
}
